<?php

/**
 * Implementation of hook_block_info();
 */
function livestreams_block_info() {
  $blocks['livestreams'] = array(
    'info' => t('livestreams'),
  );
  return $blocks;
}


/**
 * Implementation of hook_block_view();
 */
function livestreams_block_view($delta) {
  $block = array();

  switch ($delta) {
    case 'livestreams':
      $block['subject'] = t('Livestreams');
      $block['content'] = livestreams_display();
      break;
  };
  return $block;
}


/**
 * Receive Channel list and convert into usable array.
 */
function livestreams_list() {

  //Get the channel name out of the db (variable->livestreams_list)
  $namesRaw = variable_get('livestreams_stream', array());

  //Generate Array out of received string
  $namesArray = explode("\r\n", $namesRaw);

  return $namesArray;
}


/**
 * API Request and generate list of channels
 */
function livestreams_filtered() {

  //Get Livestreams array
  $livestreams = livestreams_list();

  //create new array to hold filtered data.
  $livestreamsFiltered = array();

  //API Request for each livestream
  foreach ($livestreams as $livestream) {

    //API Request
    $returnOfAPI = file_get_contents('https://api.twitch.tv/kraken/streams/' . $livestream);

    //Decode the received JSON answer of API
    $livestreamsFiltered[] = json_decode($returnOfAPI);

  }

  return $livestreamsFiltered;
}


/**
 *Generate an Array of stream information to be displayed (Name, Viewers)
 */
function livestreams_channelName() {

  //Get filtered livestreams
  $livestreams = livestreams_filtered();

  //Get Channel names of database, in case stream is offline!
  $channelNames = livestreams_list();

  //Names Array to hold every stream in a single row
  $namesArray = array();

  //Counter for Array Element
  $i = 0;

  foreach ($livestreams as $livestream) {

    //Get streamstatus (Online or offline?)
    $streamStatus = $livestream->stream;

    //Create array that holds the rows
    //Check if the stream status is equal to NULL or not.
    //If streamstatus is not null,then get proceed here.
    if (!$streamStatus == NULL) {
      $namesArray[] = array(

        //Get stream channelname and it's current viewers
        '<a href="http://twitch.tv/' . $livestream->stream->channel->name . '">' . $livestream->stream->channel->name . '</a>',
        '<span style="font-size:12px;color:#32cd32">' . $livestream->stream->viewers . '</span>'
      );
      //Increment our counter, so we know which name we need to check next
      $i++;
    }
    //If streamstatus is equal to NULL then proceed here
    else {
      $namesArray[] = array(
        //Get the channel name by the help of our counter. the API does NOT return the name in case the stream is offline.
        '<a href="http://twitch.tv/' . $channelNames[$i] . '">' . $channelNames[$i] . '</a>',
        //Instead of checking for viewers, just display the offline message in some noice crimson
        '<span style="font-size:12px;color:#dc143c">offline</span>'
      );
      //Increment our counter, so we know which name we need to check next
      $i++;
    }
  }
  return $namesArray;
}


/**
 * Display table
 * Usage of theme()
 */
function livestreams_display() {

  $output = '';
  //Get generated channels array
  $channelInfo = livestreams_channelName();

//Display the table with the correct amount of rows we require.
  $output = theme('table', array(
    'rows' => $channelInfo
  ));

//Add theme pager, I do not know what this is used for
  $output .= theme('pager');
  return $output;
}

/**
 * Implementation of hook_menu()
 */
function livestreams_menu() {
  $items = array();

  //Create a form for the module
  $items['admin/config/media/livestreams'] = array(
    'title' => 'Livestreams',
    'description' => 'Configuration of the Livestreams module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('livestreams_form'),
    'access arguments' => array('access administration page'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Generating the form of livestreams, which allows the user to add and remove streams.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function livestreams_form($form, &$form_state) {

  //Create textarea which allows to insert every name
  $form['livestream'] = array(
    '#type' => 'textarea',
    '#title' => t('Streams to display (new line for each channel!)'),
    '#cols' => 25,
    '#rows' => 10,
    '#required' => TRUE,
    '#default_value' => variable_get('livestreams_stream'),
  );

  //Submitbutton
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update'
  );

  return $form;
}

/**
 * added form_submit() feature
 * @param $form
 * @param $form_state
 */
function livestreams_form_submit($form, &$form_state) {

  //Get the current names first and display those in the textarea
  $streams = variable_get('livestreams_stream', array());

  //Update the new old list with the new values
  $streams = $form_state['values']['livestream'];
  variable_set('livestreams_stream', $streams);
}